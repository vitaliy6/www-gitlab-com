---
layout: markdown_page
title: "Labor and Employment Notices"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Since GitLab is an [all remote](/company/culture/all-remote/) company we don't have a physical worksite or breakroom wall upon which to post important labor and employment notices, so this page is our version of that.