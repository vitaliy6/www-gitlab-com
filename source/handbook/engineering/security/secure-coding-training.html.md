---
layout: markdown_page
title: "GitLab Security Secure Coding Training"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# GitLab Security Secure Coding Training

This page contains information on secure training initiatives sponsored by the GitLab Security team.

## Ruby on Rails Training with Jim Manico

### Description

The application security team would like to invite all engineers to join us in a developer focused application security training presented by Jim Manico, the creator of Brakeman, and Dr. Justin Collins.

If you are unable to participate in any portion of the training, it will be recorded for use offline. Please come with any and all questions!

When: July 29-30 9AM-5PM Eastern Time for both days
Where (Zoom): Zoom Link In Team Calendar Invite
Where (YouTube): https://www.youtube.com/watch?v=LqzDY76Q8Eo

### Schedule and Topics:

#### Day 1

9AM - NOON
(.5 hr) Introduction to Application Security
(1 hr) Threat Modeling
(.5 hr) OWASP Top Ten 2017
(.5 hr) OWASP ASVS 4.0
(.5 hr) Multi-Form Workflow Security

12:30PM - 5PM
(.5 hr) DevOps Best Practices ​
(1.5 hr) XSS Defense ​Rails (HAML)
(.5 hr) Coding Vue.js applications securely
 (1 hr) File Upload and File IO Security ​Multi-Step Secure File Upload Defense, File I/O Security
 (.5 hr) Input Validation ​Basics ​(Whitelist Validation​ and Safe Redirects)
(.5 hr) 3rd Party Library Security Management ​(​Detect and manage insecure 3rd party libraries)

#### Day 2

9AM - NOON
(.5 hr) "Dynamic render paths" / local file inclusion
(1 hr) IDOR and scoped queries
(.5 hr) SSRF Defense
(1 hr) Cross Site Request Forgery CSRF Defenses for multiple architecture types (stateless, API,web, etc)
​CSRF prevention handling
Per-form CSRF tokens
Header-based CSRF prevention (new additional option in Rails)

12:30PM - 5PM
 (1 hr) Authentication Best Practices
 (1 hr) Introduction to the OAuth authorization protocol
 (1 hr) Secure Secret Storage
​Encrypted secrets/credentials
Open Q/A and Misc Topics


Additional Information:

PowerPoint presentations: <To be added>
Burp Proxy: https://portswigger.net/burp/communitydownload
Online Labs: https://manicode.us/shepherd/
Questions Doc <Internal Only>: https://docs.google.com/document/d/1KsK5DBDgiF8k0N3cs89o1VsMYsUWUPH9fIQb_smFEac/edit
