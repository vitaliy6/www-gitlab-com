---
layout: markdown_page
title: "IAM.2.01 - Unique Identifiers Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

#  IAM.2.01 - Unique Identifiers

## Control Statement

GitLab requires unique identifiers for user accounts and prevents identifier reuse.

## Context

A unique identifier allows for every user's action to be logically separated and uniquely identified. It helps use detected and prevent abuse and suspicious activity.

## Scope

A unique identifier (UID) is a numeric or alphanumeric string that is associated with a single entity within a given system. UIDs make it possible to address that entity, so that it can be accessed and interacted with

## Ownership

Control Owner: Security Team

Process owner(s):

* Security Team: 50%
* Business Ops Team: 25%
* Compliance Team: 25%

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.2.01_unique_identifiers.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.2.01_unique_identifiers.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.2.01_unique_identifiers.md).

## Framework Mapping

* ISO
  * A.9.4.1
  * A.9.4.2
* SOC2 CC
  * CC6.1
* PCI
  * 8.1.1
  * 8.6
